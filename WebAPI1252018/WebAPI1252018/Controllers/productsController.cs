﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using WebAPI1252018.Models;

namespace WebAPI1252018.Controllers
{
    public class productsController : ApiController
    {
        Product[] products = new Product[] 
        { 
            new Product { id = 1, name = "Tomato Soup", category = "Groceries", price = 1 }, 
            new Product { id = 2, name = "Yo-yo", category = "Toys", price = 3.75M }, 
            new Product { id = 3, name = "Hammer", category = "Hardware", price = 16.99M } 
        };

        public IEnumerable<Product> GetAllProducts1252018()
        {
            return products;
        }

        public IHttpActionResult GetProduct1252018(int id)
        {
            var product = products.FirstOrDefault((p) => p.id == id);
            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }
    }
}
