﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using MVCWebAPIClient1252018.Models;

namespace MVCWebAPIClient1252018.Controllers
{
    public class usersController : ApiController
    {
        User[] users = new User[] 
        { 
            new User { id = 1, username = "1252018", password = "1252018" }, 
            new User { id = 2, username = "user_1", password = "password_1" }, 
            new User { id = 3, username = "user_2", password = "password_2" } 
        };
        // GET: api/users
        public IEnumerable<User> Get1252018()
        {
            return users;
        }

        // GET: api/users/{id}
        public User Get1252018(int id)
        {
            for (int i = 0; i < users.Length; i++)
            {
                if (users[i].id == id)
                {
                    return users[i];
                }
            }
            return null;
        }

        // GET: api/users/{username}
        public User Get1252018(string username)
        {
            for (int i = 0; i < users.Length; i++)
            {
                if (users[i].username == username)
                {
                    return users[i];
                }
            }
            return null;
        }
    }
}
