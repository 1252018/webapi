﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using MVCWebAPIClient1252018.Models;

namespace MVCWebAPIClient1252018.Controllers
{
    public class productsController : ApiController
    {
        Product[] products = new Product[] 
        { 
            new Product { id = 1, name = "Tomato Soup", category = "Groceries", price = 1 }, 
            new Product { id = 2, name = "Yo-yo", category = "Toys", price = 3.75M }, 
            new Product { id = 3, name = "Hammer", category = "Hardware", price = 16.99M } 
        };
        // GET: api/products
        public IEnumerable<Product> Get1252018()
        {
            return products;
        }

        // GET: api/products/{id}
        public Product Get1252018(int id)
        {
            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].id == id)
                {
                    return products[i];
                }
            }
            return null;
        }

        //GET: api/products?category={categoryValue}
        public Product Get1252018(string categoryValue)
        {
            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].category == categoryValue)
                {
                    return products[i];
                }
            }
            return null;
        }
    }
}
